"""This is an example Python script of a possible implementation of data integration with The Write Class API.
This script takes advantage of a Python OAuth2 client library (requests_oauthlib) to simplify the authentication
process. It should work in both Python 2 & 3.
To use it first install the packages specified in the requirements.txt:

  $ pip install -r requirements.txt

To run:

  $ python python_example.py
"""
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from datetime import datetime, timedelta

"""From the documentation, this is where the Base Integration URL would go
An example of this that corresponds to the documentation would be:

  api_url = 'https://twc.dev'
"""
api_url = '[Base Integration URL]'

"""Change the client_id and client_secret variables to your specific credentials provided by the admin Integrations tab
"""
client_id = '123456789'
client_secret = '987654321'

"""This initializes the oauth2 client with your client_id
"""
client = BackendApplicationClient(client_id=client_id)
oauth = OAuth2Session(client=client)

"""Fetch an access token from the API using the credentials (client_id and client_secret) provided to you previously.
This access token can be used for an hour and then it expires. At which point you will need to fetch a new token.

Headers:
  Content-Type: Specify the type of content sent to the API. This is automatically set to 
    'application/x-www-form-urlencoded' by the requests_oauthlib library

Body Parameters:
  grant_type: this is the type of grant and automatically set to 'client_credentials' by the requests_oauthlib library
  
  token_url: the URI to the API endpoint that returns the access token
  
  client_id: your unique client ID provided by the Admin UI
  
  client_secret: your unique client secret provided by the Admin UI
  
  scope: a list of the permissions allowed by the access token. 'read' is the only option currently

Returns:
  {
      "access_token": "your_access_token",
      "token_type": "Bearer",
      "scope": "read"
  }
"""
access_token = oauth.fetch_token(
  token_url=api_url + '/oauth2/token',
  client_id=client_id,
  client_secret=client_secret,
  scope=['read'],
  verify=False,
)

"""Set up dates & times to be used in export call below

Variables:
  yesterday_at_midnight = Yesterday @ 12:00AM - this will be the lower bound for response records
  today_at_midnight = Today @ 12:00AM - this will be the upper bound for response records
"""
one_day = timedelta(days=1)
now = datetime.now()
today_at_midnight = datetime(now.year, now.month, now.day)
yesterday_at_midnight = today_at_midnight - one_day

"""This request will export the student response records from the API based on the parameters passed to the endpoint.
Records are returned in JSON format.

Query Parameters:
  done (boolean, optional): whether or not to the return student response records that are done or finished and a course
    placement has been defined in the record.

    Options:
      True: only returns response records that have been finished/completed
      False: only returns response records that are unfinished
      Default: returns all response records regardless of completion status

  invalid (boolean, optional): whether or not to return student response records that have been marked by an advisor/admin as
    invalid. Invalid can mean the record is incomplete or that it contains errors.

    Options:
      True: only returns response records that are invalid
      False: only returns response records that are valid
      Default: returns all response records regardless of validity

  archived (boolean, optional): whether or not to return student response records that have been marked by an advisor/admin as
    archived.

    Options:
      True: only returns response records that have been marked as archived
      False: only returns response records that have not been archived
      Default: returns all response records regardless of archive status

  updated_start (datetime, optional): All records that were updated on or after this time will be returned. This can be
    used with the updated_end parameter to specify both lower and upper bounds. By default, there is no lower bound
    on updated time in place.

    Example:
      updated_start: 2018-01-03T00:54:24.001Z
      Returns: All records updated on or after January 03, 2018 12:54:24.001 AM (inclusive)

  updated_end (datetime, optional): All records that were updated before this time will be returned. This can be used with the
    updated_start parameter to specify both upper and lower bounds. By default, there is no upper bound on updated time
    in place

    Example:
      updated_end: 2018-01-03T00:54:24.001Z
      Returns: All records updated before January 03, 2018 12:54:24.001 AM (exclusive)

Headers:
  Authorization: this is where the access token is sent. This is automatically handled by the requests_oauthlib
    library.
    
    Format:
      Authorization: Bearer <access_token>
    
    Example:
      Authorization: Bearer asdfasdfalskdjfalksdjfasdf

Returns (example):
  [
    {
      "id": 1,
      "created_at": "2018-04-30T14:48:48.673Z",
      "updated_at": "2018-05-01T20:59:32.790Z",
      "user_id": 2,
      "schema_version_id": 1,
      "response": {
        "final_result_id": "english_class_100"
      },
      "done": true,
      "needs_review": false,
      "approved": null,
      "invalid": false,
      "archived": false,
      "user": {
        "student_id": "55555555",
        "account_name": "test1",
        "first_name": "test",
        "last_name": "student",
        "email": "test@email.edu"
      }
    }
  ]
"""
response = oauth.get(api_url + '/export.json?&done={done}&invalid={invalid}&archived={archived}'
                     '&updated_start={updated_start}&updated_end={updated_end}'.format(
                        done=True,
                        invalid=False,
                        archived=False,
                        updated_start=yesterday_at_midnight,
                        updated_end=today_at_midnight,
                      ))
print(response.json())
