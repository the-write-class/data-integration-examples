# The Write Class Web Export API

## Overview

The purpose of The Write Class web export API is to allow integrations with registration systems to make it a smooth process to use course placement from The Write Class.

The web-based export API will allow getting data about student responses from the system in multiple data formats on demand. Then it will be up to the integrator to take the data, transform into a desired shape or format, and push into the desired system to be integrated. This allows for a secure system as The Write Class application does not need to know anything about the systems that are being integrated with, so it does not need to store or hold any special credentials for the user's infrastructure.

Internal end user IT servers can be used to pull the data from The Write Class export API and push to the integration system completely inside of the IT firewall. The only connection to the outside world required is an outgoing web API request to The Write Class API servers.

## Setup

Within The Write Class admin interface, there is an "Integrations" tab:

![Integrations tab](images/integrations-tab.png)

The content will have a few components to it:

![Integrations tab contents](images/integrations-tab-contents.png)

* Base Integration URL
* Add a new Client
* List of all Clients

The `Base Integration URL` is the web URL to your API that will be used in creating a script to pull data from the API.  It will not match the URL shown in the picture, it will be specific to your application and will always be `https` for security reasons.

The first step is to create a Client.  Specify the name and click the Add + button.

![Add Client](images/add-client.png)

The resulting popup will list two pieces of info that you will need, along with the `Base Integration URL` mentioned previously, to get data from the API.

![Add Client Credentials](images/add-client-creds.png)

The two pieces of info are your credentials to connect ot the API:
* Client Id
* Client Secret

As the popup mentions, you must save the `Client Secret` somewhere.  For security reasons, after the popup is closed, it will never be viewable again.  If you do happen to lose it, just create a new Client as there is no way to recover an old one.

Authentication with the web API is accomplished using the 3 pieces of info and will be used in later steps:
* Base Integration URL
* Client Id
* Client Secret

You will now be able to see your Client that you created in the list.

![Client List](images/client-list.png)

## Web API Endpoint details

Keep in mind in all of the examples below, replace [Base Integration URL] including the square brackets with the URL that you got from the previous steps, in our example it would be `https://twc.dev`.

**API endpoint**
```
[Base Integration URL]/export.TYPE?&updated_start=DATE&updated_end=DATE&done=DONE&invalid=INVALID&archived=ARCHIVED
```
**TYPE - required text extension**

   `TYPE` is one of:
   * xlsx
   * csv
   * json

This is a suffix or extension for the export and dictates the format type of the data from the server. Examples are included below the parameter descriptions.  We expect that JSON is the more flexible format for an integration as it has great support in any language and does not have issues with values with commas like CSV would.

The CSV and XLSX types will return a 302 redirect so make sure that whatever method you are using will follow redirects.

**updated_start and updated_end - optional date fields**

The `updated_start` and `updated_end` DATE fields will return records that were updated greater than or equal to updated_start and less than updated_end.

The date must be in this format (ISO 8601 Notation):

yyy-mm-ddThh:mm:ssZ

Example:

2018-01-03T00:54:24Z

While these fields are optional, it is recommended that it be utilized. The design of this date range in the API will allow you to run the script, for example, daily to get only records that have been updated within the past day.

One strategy is to get the date the last time the script was run to use as updated_start, then grab the current time and pass in as updated_end. Then save off updated_end to use the next time the script runs. That way, all time periods are covered and you should not miss any records.

**done - optional boolean field**

The `done` field will return records that are done. These records could still be pending approval of an application, but are considered done because a course has been selected. DONE is a boolean field, and is optional. If not included, all records, done or not done, will be included. Otherwise `done` can be:
   * true
   * false

**invalid - optional boolean field**

The `invalid` field will return records that are invalid. These records can be at any stage of the process but are considered to either be incomplete or contain errors. This is a boolean field, and is optional. If not included, all records, invalid or valid, will be included. Otherwise `invalid` can be:
   * true
   * false

**archived - optional boolean field**

The `archived` field will return records that have been marked as archived. These records still are stored alongside unarchived records but are considered to be old and possibly outdated. This is a boolean field, and is optional. If not included, all records, archived or not archived, will be included. Otherwise `archived` can be:
   * true
   * false

### Examples

For most practical uses, the values you will want to use for DONE, INVALID, and ARCHIVED are as follows:

* DONE = true
* INVALID = false
* ARCHIVED = false

as demonstrated in this sample request for JSON data:

```
[Base Integration URL]/export.json?&updated_start=2018-04-30T14:48:48Z&updated_end=2018-05-03T20:59:32Z&done=true&invalid=false&archived=false
```

**Response**
```
[
  {
    "id": 1,
    "created_at": "2018-04-30T14:48:48.673Z",
    "updated_at": "2018-05-01T20:59:32.790Z",
    "user_id": 2,
    "schema_version_id": 1,
    "response": {
      "final_result_id": "english_class_100"
    },
    "done": true,
    "needs_review": false,
    "approved": null,
    "invalid": false,
    "archived": false,
    "user": {
      "student_id": "55555555",
      "account_name": "Testuser",
      "first_name": "test",
      "last_name": "User",
      "email": "testuser@email.edu"
    }
  }
]
```

#### XLSX/CSV Request
```
[Base Integration URL]/export.csv?&updated_start=2018-04-30T14:48:48Z&updated_end=2018-05-03T20:59:32Z&done=true&invalid=false&archived=false
```

This will return a .csv (or .xlsx) file that will be a representation of the above JSON example but flattend with double underscores.  Reminder, a CSV or XLSX request returns a 302 redirect so make sure you follow that redirect.

Here are some screenshots of what the sample data might look like:

![Export data sample 1](images/image1.png)

![Export data sample 2](images/image2.png)

Notice the JSON path:

response.final_result_id

becomes the column name:

response__final_result_id

The nesting in the JSON structure is represented by double underscores.

**Selected Course**

For all types of data representations, the "done" flag indicates whether the assessment is complete. The "response.final_result_id" field is the course that is the completed course.

### Authentication

Keep in mind in all of the examples below, replace [Base Integration URL] including the square brackets with the URL that you got from the previous steps, in our example it would be `https://twc.dev`.

We use OAuth2 authentication to access the API.
1. Get credentials for your application in the Admin UI. The Admin UI will generate a client_id and client_secret for you.
    ```
    client_id = 123456789
    client_secret = 987654321
    ```
2. Use these credentials to fetch an access token from the API.

    ```
    $ curl --location --request POST '[Base Integration URL]/oauth2/token' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data-urlencode 'grant_type=client_credentials' \
    --data-urlencode 'client_id=123456789' \
    --data-urlencode 'client_secret=987654321' \
    --data-urlencode 'scope=read'
    ```

Response:

    
    {
      "access_token": "alkdjf3298kajf298kjhkjgh",
      "token_type": "Bearer",
      "scope": "read",
      "expires_in": 3600
    }
    
    
The access token received can be used for one hour at which point it will expire. You will then need to fetch a new token using your credentials.

3. Use the `access_token` to access the /export endpoint
    
    The `access_token` is inserted in the *Authorization* header in this format:
        
      Authorization: Bearer `access_token`

    I add the `--location` in to follow a redirect.  In the case of the json type it isn't necessary, but with csv or xlsx we return a 302 redirect to the proper resource.

    ```
    $ curl --location --request GET -H "Authorization: Bearer alkdjf3298kajf298kjhkjgh" "[Base Integration URL]/export.json?done=true&invalid=false&archived=false"

    [
        {
            "id": 1,
            "created_at": "2018-04-30T14:48:48.673Z",
            "updated_at": "2018-05-01T20:59:32.790Z",
            "user_id": 2,
            "schema_version_id": 1,
            "response": {
                "final_result_id": "english_class_100"
            },
            "done": true,
            "needs_review": false,
            "approved": null,
            "invalid": false,
            "archived": false,
            "user": {
                "student_id": "55555555",
                "account_name": "Testuser",
                "first_name": "test",
                "last_name": "User",
                "email": "testuser@email.edu"
            }
        }
    ]
    ```

There are language-specific examples in the examples/ folder.
